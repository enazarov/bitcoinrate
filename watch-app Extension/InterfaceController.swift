//
//  InterfaceController.swift
//  watch-app Extension
//
//  Created by Eugene Nazarov on 03.12.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import WatchKit
import Foundation

class InterfaceController: WKInterfaceController {

    var viewModel: ConcreteHistoricalDataViewModel?
    @IBOutlet weak var table: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        let appCore = AppCore(appConfiguration: AppConfiguration.testing)
        viewModel = ConcreteHistoricalDataViewModel(appCore: appCore)
    }
    
    override func willActivate() {
        super.willActivate()
        viewModel?.delegate = self
        viewModel?.enter()
    }
    
    override func didDeactivate() {
        viewModel?.leave()
        super.didDeactivate()
    }
    
    func refreshTable() {
        guard let dataArray = viewModel?.historicalData else { return }
        table.setNumberOfRows(dataArray.count, withRowType: "TableRow")
        for (index, data) in dataArray.enumerated() {
            guard let row = table.rowController(at: index) as? TableRowController else { return }
            row.label.setText(data.date + ": " + data.rate)
        }
    }

}

extension InterfaceController: HistoricalDataViewModelDelegate {
    
    func viewModelDidUpdateCurrentRate(_ viewModel: HistoricalDataViewModel) {}
    
    func viewModelDidUpdateHistoricalData(_ viewModel: HistoricalDataViewModel) {
        refreshTable()
    }
}
