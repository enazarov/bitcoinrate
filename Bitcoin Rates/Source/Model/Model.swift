//
//  ExchangeRate.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 04.09.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import Foundation

typealias JSONDictionary = [AnyHashable:Any]

struct Model {
    
    static var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }
    
    struct ExchangeRate {
        let price: Double
        let date: Date
    }
    
    struct DailyRate {
        let average: Double
        let date: Date
    }
}


extension Model.ExchangeRate {
    init?(json: JSONDictionary) {
        guard
            let price = json["price"] as? Double,
            let time = json["time"] as? String,
            let date = Model.dateFormatter.date(from: time) else { return nil }
        self.price = price
        self.date = date
    }
}

extension Model.DailyRate {
    init?(json: JSONDictionary) {
        guard
            let average = json["average"] as? Double,
            let time = json["time"] as? String,
            let date = Model.dateFormatter.date(from: time) else { return nil }
        self.average = average
        self.date = date
    }
}
