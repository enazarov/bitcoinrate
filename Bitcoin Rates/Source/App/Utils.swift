//
//  Utils.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 05.09.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import UIKit
import Foundation

struct Fonts {
    
    static let `default` = UIFont(name: "AvenirNext-Regular", size: 14)
    static let medium = UIFont(name: "AvenirNext-DemiBold", size: 18)
    static let large = UIFont(name: "AvenirNext-DemiBold", size: 36)
}

struct Log {
    static func message(_ string: String)  {
        NSLog(string)
    }
}

struct Strings {
    static let exchangeRateNotAvailable = "--"
    static let exchangeRateLoading = "Loading..."
    static let exchangeRateError = "Something went wrong."
}
