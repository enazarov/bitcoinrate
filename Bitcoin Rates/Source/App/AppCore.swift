//
//  AppCore.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 04.09.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import Foundation

enum AppMode {
    case testing, live
}

struct AppConfiguration {
    
    let apiBaseURL: String
    let mode: AppMode
    
    static var live: AppConfiguration {
        return AppConfiguration(apiBaseURL: "https://apiv2.bitcoinaverage.com/", mode: .live)
    }
    
    static var testing: AppConfiguration {
        return AppConfiguration(apiBaseURL: "https://example.com", mode: .testing)
    }
}

enum Currency: String {
    case usd = "USD"
    case euro = "EUR"
    case btc = "BTC"
}

struct Preferences {
    let currency: Currency
}

final class AppCore {
    
    private let appConfiguration: AppConfiguration
    let preferences: Preferences
    let apiClient: APIClient
    
    init(appConfiguration: AppConfiguration) {
        self.appConfiguration = appConfiguration
        self.preferences = Preferences(currency: .euro)
        if appConfiguration.mode == .testing {
            self.apiClient = MockAPIClient()
        } else {
            let url = URL(string: appConfiguration.apiBaseURL)!
            let webService = ConcreteWebService(with: URLSession.shared, baseURL: url)
            self.apiClient = ConcreteAPIClient(with: webService, preferences: preferences)
        }
    }
    
}
