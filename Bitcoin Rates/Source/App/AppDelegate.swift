//
//  AppDelegate.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 04.09.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCore: AppCore!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupApp()
        setupWindow()
        showMainScreen()
        
        return true
    }

    private func setupApp() {
        appCore = AppCore(appConfiguration: AppConfiguration.testing)
    }
    
    private func setupWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
    }
    
    private func showMainScreen() {
        let viewModel = ConcreteHistoricalDataViewModel(appCore: appCore)
        let rootViewController = HistoricalDataViewController(with: viewModel)
        window?.rootViewController = UINavigationController(rootViewController: rootViewController)
        window?.makeKeyAndVisible()
    }
}
