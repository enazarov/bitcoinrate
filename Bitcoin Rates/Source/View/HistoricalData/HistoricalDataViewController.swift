//
//  HistoricalDataViewController.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 03.12.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import UIKit

class HistoricalDataViewController: UITableViewController {

    private let viewModel: HistoricalDataViewModel
    
    private var tableHeaderViewId = "TableHeaderViewId"

    init(with viewModel: HistoricalDataViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.currencyString
        tableView.register(TableHeaderView.self, forHeaderFooterViewReuseIdentifier: tableHeaderViewId)
        tableView.estimatedSectionHeaderHeight = 60
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.delegate = self
        viewModel.enter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.delegate = nil
        viewModel.leave()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.historicalData?.count ?? 0
    }

    private var cellID = "Cell"
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: cellID)
        
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellID)
        }
        
        let data = viewModel.historicalData![indexPath.row]
        
        cell?.textLabel?.text = data.rate
        cell?.detailTextLabel?.text = data.date
        
        return cell!
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let data = viewModel.currentRate ,let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: tableHeaderViewId) as? TableHeaderView {
            view.priceLabel.text = data.rate
            view.dateLabel.text = data.date
            return view
        }
        
        return nil
    }
}

extension HistoricalDataViewController: HistoricalDataViewModelDelegate {
    func viewModelDidUpdateCurrentRate(_ viewModel: HistoricalDataViewModel) {
        tableView.reloadData()
    }
    
    func viewModelDidUpdateHistoricalData(_ viewModel: HistoricalDataViewModel) {
        tableView.reloadData()
    }
}
