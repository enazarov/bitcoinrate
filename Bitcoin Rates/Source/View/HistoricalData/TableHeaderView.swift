//
//  TableHeaderView.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 03.12.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import UIKit

class TableHeaderView: UITableViewHeaderFooterView {

    let priceLabel = UILabel()
    let dateLabel = UILabel()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    private func configureView() {
        contentView.addSubview(priceLabel)
        contentView.addSubview(dateLabel)
        
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addConstraints([
            priceLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            priceLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            priceLabel.bottomAnchor.constraint(equalTo: dateLabel.topAnchor, constant: 7),
            dateLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            dateLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12)
        ])
        
        priceLabel.textAlignment = .center
        dateLabel.textAlignment = .center
        
        priceLabel.font = Fonts.large
        dateLabel.font = Fonts.medium
    }
    
}
