//
//  HistoricalDataViewModel.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 05.09.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import Foundation

struct HistoricalData {
    let rate: String
    let date: String
}

extension HistoricalData {
    
    init(with dailyRate: Model.DailyRate) {
        let rate = String(format:"%.2f", dailyRate.average)
        let date = DateFormatter.localizedString(from: dailyRate.date, dateStyle: .short, timeStyle: .none)
        self.init(rate: rate, date: date)
    }
    
    init(with exchangeRate: Model.ExchangeRate) {
        let rate = String(format:"%.2f", exchangeRate.price)
        let date = DateFormatter.localizedString(from: exchangeRate.date, dateStyle: .none, timeStyle: .short)
        self.init(rate: rate, date: date)
    }
    
}

protocol HistoricalDataViewModel: class {
    
    var delegate: HistoricalDataViewModelDelegate? { get set }
    var currencyString: String { get }
    var currentRate: HistoricalData? { get }
    var historicalData: [HistoricalData]? { get }
    
    func enter()
    func leave()
}

protocol HistoricalDataViewModelDelegate: class {
    func viewModelDidUpdateCurrentRate(_ viewModel: HistoricalDataViewModel)
    func viewModelDidUpdateHistoricalData(_ viewModel: HistoricalDataViewModel)
}

class ConcreteHistoricalDataViewModel: HistoricalDataViewModel {

    private let appCore: AppCore
    private let refreshInterval: TimeInterval = 60
    private var refreshTimer: Timer?
    private let daysCount = 14
    
    
    var currentRate: HistoricalData? {
        didSet {
            delegate?.viewModelDidUpdateCurrentRate(self)
        }
    }
    
    var historicalData: [HistoricalData]? {
        didSet {
            delegate?.viewModelDidUpdateHistoricalData(self)
        }
    }
    
    weak var delegate: HistoricalDataViewModelDelegate?
    
    let currencyString: String
    
    init(appCore: AppCore) {
        self.appCore = appCore
        self.currencyString = "\(Currency.btc.rawValue)/\(appCore.preferences.currency.rawValue)".uppercased()
    }
    
    func enter() {
        updateExchangeRate()
        updateData()
        startTimer()
    }
    
    func leave() {
        stopTimer()
    }
    
    private func startTimer() {
        stopTimer()
        refreshTimer = Timer.scheduledTimer(timeInterval: refreshInterval, target: self, selector: #selector(updateExchangeRate), userInfo: nil, repeats: true)
    }
    
    private func stopTimer() {
        refreshTimer?.invalidate()
        refreshTimer = nil
    }
    
    @objc
    private func updateExchangeRate() {
        appCore.apiClient.getCurrentRate { exchangeRate in
            if let exchangeRate = exchangeRate {
                self.currentRate = HistoricalData(with: exchangeRate)
            }
        }
    }
    
    private func updateData() {
        appCore.apiClient.getHistoricalData { rates in
            if let rates = rates {
                self.historicalData = rates[..<self.daysCount].map(HistoricalData.init)
            } else {
                self.historicalData = nil
            }
        }
    }
}

