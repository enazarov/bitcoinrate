//
//  APIClient.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 04.09.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import Foundation

protocol APIClient {
    
    func getCurrentRate(completion: @escaping ((Model.ExchangeRate?) -> Void))
    func getHistoricalData(completion: @escaping (([Model.DailyRate]?) -> Void))
    
}

extension Model.ExchangeRate {
    
    static func current(parameters: [String: String]?) -> Resource<Model.ExchangeRate> {
        return Resource<Model.ExchangeRate>(action: "convert/global", parameters: parameters, parseJSON: { json -> Model.ExchangeRate? in
            guard let dict = json as? JSONDictionary else { return nil }
            return Model.ExchangeRate(json: dict)
        })
    }

}

extension Model.DailyRate {
    
    static func historicalData(for currency: Currency, with parameters: [String: String]?) -> Resource<[Model.DailyRate]> {
        return Resource<[Model.DailyRate]>(action: "indices/global/history/\(Currency.btc.rawValue)\(currency.rawValue)", parameters: parameters, parseJSON: { json -> [Model.DailyRate]? in
            guard let dicts = json as? [JSONDictionary] else { return nil }
            return dicts.flatMap(Model.DailyRate.init)
        })
    }
    
}

final class ConcreteAPIClient: APIClient {
    
    let webService: WebSerivce
    let preferences: Preferences
    
    init(with webService: WebSerivce, preferences: Preferences) {
        self.webService = webService
        self.preferences = preferences
    }
    
    func getCurrentRate(completion: @escaping ((Model.ExchangeRate?) -> Void)) {
        let resource = Model.ExchangeRate.current(parameters: currentRateParameters())
        webService.loadResource(resource, callback: completion)
    }
    
    func getHistoricalData(completion: @escaping (([Model.DailyRate]?) -> Void)) {
        let resource = Model.DailyRate.historicalData(for: preferences.currency, with: historicalDataParameters())
        webService.loadResource(resource, callback: completion)
    }
    
    private func currentRateParameters() -> [String: String] {
        return ["from": Currency.btc.rawValue,
                "to":preferences.currency.rawValue,
                "amount":"1"]
    }
    
    private func historicalDataParameters() -> [String: String] {
        return ["period": "alltime",
                "format":"json"]
    }
}

final class MockAPIClient: APIClient {
    
    func getHistoricalData(completion: @escaping (([Model.DailyRate]?) -> Void)) {
        if
            let path = Bundle.main.path(forResource: "BTCEUR", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)),
            let json = try? JSONSerialization.jsonObject(with: data, options: []),
            let dicts = json as? [JSONDictionary] {
            
            completion(dicts.flatMap(Model.DailyRate.init))
            return
        }
        
        completion(nil)
    }

    func getCurrentRate(completion: @escaping ((Model.ExchangeRate?) -> Void)) {
        let json = [
            "success": true,
            "time": "2017-09-06 17:14:00",
            "price": 3872.26
            ] as JSONDictionary
        
        completion(Model.ExchangeRate(json: json))
    }
    
}
