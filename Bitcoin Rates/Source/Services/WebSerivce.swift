//
//  WebSerivce.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 04.09.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import Foundation

struct Resource<A> {
    
    let action: String
    let parameters: [String: String]?
    let parse: (Data) -> A?
    
}

extension Resource {
    
    init(action: String, parameters: [String: String]?, parseJSON: @escaping (Any) -> A?) {
        self.action = action
        self.parameters = parameters
        self.parse = { data in
            let json = try? JSONSerialization.jsonObject(with: data, options: [])
            return json.flatMap(parseJSON)
        }
    }
    
}

protocol WebSerivce {
    
    func loadResource<A>(_ resource: Resource<A>, callback: @escaping (A?) -> ())
    
}

final class ConcreteWebService: WebSerivce {
    
    private let urlSession: URLSession
    private let baseURL: URL
    
    init(with urlSession: URLSession, baseURL: URL) {
        self.urlSession = urlSession
        self.baseURL = baseURL
    }
    
    func loadResource<A>(_ resource: Resource<A>, callback: @escaping (A?) -> ()) {
        guard let url = createUrlForResource(resource) else { return }
        urlSession.dataTask(with: url) { (data, response, error) in
            
            if let httpResponse = response as? HTTPURLResponse {
                Log.message("Did load request at \(url.absoluteString) with status code: \(httpResponse.statusCode)")
            }
            
            let result = data.flatMap(resource.parse)
            DispatchQueue.main.async {
                callback(result)
            }
            }.resume()
    }
    
    private func createUrlForResource<A>(_ resource: Resource<A>) -> URL? {
        let url = baseURL.appendingPathComponent(resource.action)
        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) {
            urlComponents.queryItems = queryItems(dictionary: resource.parameters)
            return urlComponents.url
        }
        
        return nil
    }
    
    func queryItems(dictionary: [String: String]?) -> [URLQueryItem] {
        if let dict = dictionary {
            return dict.map {
                URLQueryItem(name: $0, value: $1)
            }
        }
        
        return []
    }
}
