# README #

### Bitcoin Price Index App ###

A little iOS App that allows its users to get information on the current bitcoin price, as well as to have a look at the historic changes to the price. 

![UI](https://bitbucket.org/enazarov/bitcoinrate/downloads/UI.png)
