//
//  ModelTests.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 04.09.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import XCTest

class ModelTests: XCTestCase {
    
    func testExchangeRateParsing() {
        let json = [
            "success": true,
            "time": "2016-04-14 13:55:32",
            "price": 424.93
        ] as JSONDictionary
        
        if let exchangeRate = Model.ExchangeRate(json: json) {
            XCTAssert(exchangeRate.price == 424.93, "Invalid ExchangeRate price")
            XCTAssert(exchangeRate.date == Date(timeIntervalSince1970: 1460642132), "Invalid ExchangeRate date")
        } else {
            XCTFail("Cannot serealize ExchangeRate object")
        }
    }
    
    func testHistoricalDataParsing() {
        let json = ["time": "2016-04-14 13:55:32","average": 3872.26] as JSONDictionary
        
        if let rate = Model.DailyRate(json: json) {
            XCTAssert(rate.average == 3872.26, "Invalid DailyRate average")
            XCTAssert(rate.date == Date(timeIntervalSince1970: 1460642132), "Invalid DailyRate date")
        } else {
            XCTFail("Cannot serealize DailyRate object")
        }
    }
    
}

