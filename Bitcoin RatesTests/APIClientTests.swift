//
//  APIClientTests.swift
//  Bitcoin Rates
//
//  Created by Eugene Nazarov on 04.09.17.
//  Copyright © 2017 Evgeny Nazarov. All rights reserved.
//

import XCTest

class APIClientTests: XCTestCase {
    
    let appCore = AppCore(appConfiguration: AppConfiguration.live)
    
    func testGetCurrentRate() {
        let expectation = XCTestExpectation(description: "Download current rate")
        
        appCore.apiClient.getCurrentRate { exchaneRate in
            XCTAssertNotNil(exchaneRate, "No data was downloaded.")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 3)
    }
    
    func testGetHistoricalData() {
        let expectation = XCTestExpectation(description: "Download historical data")
        
        appCore.apiClient.getHistoricalData { exchaneRates in
            XCTAssertNotNil(exchaneRates, "No data was downloaded.")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 3)
    }
    
}
